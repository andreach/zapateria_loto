﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COMMON.Entidades
{
    public class Ventas
    {
        public string IdEmpleado { get; set; }
        public string IdCategoria { get; set; }
        public DateTime Fecha { get; set; }
    }
}
