﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COMMON.Entidades
{
    public class Empleados
    {
        public string NombreEmpleado { get; set; }
        public string ApellidosEmpleado {get; set;}
        public string Puesto { get; set; }
        public string Direccion { get; set; }
        public string NumTelefono { get; set; }
        public double Sueldo { get; set; }
        public override string ToString()
        {
            return $"{NombreEmpleado} - {ApellidosEmpleado}";
        }

    }
}
