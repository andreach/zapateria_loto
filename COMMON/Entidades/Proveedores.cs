﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COMMON.Entidades
{
    public class Proveedores
    {
        public string NombreProveedor { get; set; }
        public string ApellidosProveedor { get; set; }
        public string Direccion { get; set; }
        public string NumTelefono { get; set; }
        public string Correo { get; set; }
        public override string ToString()
        {
            return $"{NombreProveedor}";
        }

    }
}
