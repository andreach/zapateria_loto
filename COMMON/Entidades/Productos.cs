﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COMMON.Entidades
{
    public class Productos
    {
        public string NombreProducto { get; set; }
        public string Descripcion { get; set; }
        public string IdCategoria { get; set; }
        public string IdProveedor { get; set; }
        public double Precio { get; set; }
        public override string ToString()
        {
            return $"{NombreProducto}";
        }

    }
}
